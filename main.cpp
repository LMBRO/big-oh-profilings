/*
 * main.cpp
 *
 *  Created on: 2017-02-12
 *      Author: etudiant
 */
#include <functional>
#include <iostream>
#include <ctime>
#include <vector>
unsigned int f1(unsigned int n) {
	unsigned int sum = 0;
	for (unsigned int i = 0; i < n; ++i) {
		sum++;
	}
	return sum;
}
unsigned int f2(unsigned int n) {
	unsigned int sum = 0;
	for (unsigned int i = 0; i < n; ++i)
		for (unsigned int j = 0; j < n; ++j)
			++sum;
	return sum;
}
unsigned int f3(unsigned int n) {
	unsigned int sum = 0;
	for (unsigned int i = 0; i < n; ++i)
		for (unsigned int j = 0; j < n * n; ++j)
			++sum;
	return sum;
}
unsigned int f4(unsigned int n) {
	unsigned int sum = 0;
	for (unsigned int i = 0; i < n; ++i)
		for (unsigned int j = 0; j < i; ++j)
			++sum;
	return sum;
}
unsigned int f5(unsigned int n) {
	unsigned int sum = 0;
	for (unsigned int i = 0; i < n; ++i)
		for (unsigned int j = 0; j < i * i; ++j)
			for (unsigned int k = 0; k < j; ++k)
				++sum;
	return sum;
}
void profile(std::function<unsigned int(unsigned int)> const & f) {
	std::vector<unsigned int> v = { 10,20,40,80,100,110 };

	for (unsigned int N : v) {
		std::clock_t c_start = std::clock();
		f(N);
		std::clock_t c_end = std::clock();
		std::cout << 1000.0 * (c_end - c_start) / CLOCKS_PER_SEC << "ms\t";
	}
}
int main() {
	std::function<unsigned int(unsigned int)> fn1 = f1; //O(N)
	std::function<unsigned int(unsigned int)> fn2 = f2; //O(N²)
	std::function<unsigned int(unsigned int)> fn3 = f3; //O(N³)
	std::function<unsigned int(unsigned int)> fn4 = f4; //O(N²)
	std::function<unsigned int(unsigned int)> fn5 = f5; //O(N⁵)
	profile(fn5);
	return 0;
}

